package com.lastplay.batotorssreader.models;

import com.orm.SugarRecord;

/**
 * Created by Harits Elfahmi on 2/20/2015.
 */
public class Item extends SugarRecord<Item>{
    public String title;
    public String link;
    public String pubDate;
    public String description;
    public String status;

    public User user;

    public Item(){}

    public Item(String title, String link, String pubDate, String description, String status, User user) {
        this.title = title;
        this.link = link;
        this.pubDate = pubDate;
        this.description = description;
        this.status = status;
        this.user = user;
    }
}