package com.lastplay.batotorssreader.models;

import com.orm.SugarRecord;

/**
 * Created by Harits Elfahmi on 2/20/2015.
 */
public class User extends SugarRecord<User>{
    public String username;
    public String secretKey;
    public Boolean isDefault;

    public User(){}


    public User(String username, String secretKey){
        this.username = username;
        this.secretKey = secretKey;
        this.isDefault = false;
    }
}
