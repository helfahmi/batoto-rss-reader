package com.lastplay.batotorssreader;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.lastplay.batotorssreader.models.Item;

/**
 * Created by Harits Elfahmi on 4/9/2015.
 */
public class UpdateManager {

    private int notificationId = 1;
    private NotificationCompat.Builder mBuilder;
    private Context context;
    private static UpdateManager mInstance = null;

    private UpdateManager(Context context) {
        this.context = context;
        mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("My notification")
                .setContentText("Hello World!");
    }

    public static UpdateManager getInstance(Context context){
        if(mInstance == null){
            mInstance = new UpdateManager(context);
        }
        return mInstance;
    }

    public void notify(Item item) {
        // Gets an instance of the UpdateManager service
        NotificationManager mNotifyMgr = (android.app.NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        mBuilder.setContentTitle(item.title);
        mBuilder.setContentText(item.pubDate);
        // Builds the notification and issues it.
        mNotifyMgr.notify(notificationId, mBuilder.build());
    }
}
