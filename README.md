# README #

Comic updates management application for Batoto comic site using its "My Follow" RSS data.

### Feature ###

* Mark updates as completed, add to queue, or ignore it, with swiping 
* Updates are saved to your phone, so you do not always need internet connection
* Get push notification to your phone for each new updates

### Libraries ###

* Android Async HTTP by loopj
* SugarORM by satyan
* ListViewAnimation by nhaarman

© 2015 Harits Elfahmi