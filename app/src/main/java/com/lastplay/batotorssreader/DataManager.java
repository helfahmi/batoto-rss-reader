package com.lastplay.batotorssreader;

import android.util.Xml;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.lastplay.batotorssreader.models.Action;
import com.lastplay.batotorssreader.models.Item;
import com.lastplay.batotorssreader.models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Harits Elfahmi on 2/20/2015.
 */
public class DataManager {

    private String defaultSecretKey = "86bdc848bab462e7a9ef2cfbc43c5225";
    private static DataManager instance = new DataManager();
    private ArrayList<Item> items;
    private User currentUser = null;
    private ListView comicListView;
    private ArrayAdapter<String> adapter;

    private DataManager(){}

    public static DataManager getInstance(){
        return instance;
    }

    public User getCurrentUser() throws Exception{
        if (currentUser != null){
            return currentUser;
        } else {
            List<User> users = User.find(User.class, "is_default = ?", "1");
            if(users.isEmpty()){
                throw new Exception("User is not set yet.");
            } else {
                User defaultUser = users.get(0);
                setCurrentUser(defaultUser);
                return defaultUser;
            }
        }
    }

    public boolean currentUserExist(){
        List<User> users = User.find(User.class, "is_default = ?", "1");
        if(users.isEmpty()){
            return false;
        } else {
            return true;
        }
    }

    public void setViewData(final ListView comicListView, final ArrayAdapter<String> adapter){
        this.comicListView = comicListView;
        this.adapter = adapter;
    }


    public void setCurrentUser(User curUser){
        //set all user to default false
        List<User> users = User.listAll(User.class);
        for(User user: users){
            user.isDefault = false;
            user.save();
        }

        curUser.isDefault = true;
        curUser.save();
        currentUser = curUser;
    }

    public String getDefaultSecretKey(){
        return defaultSecretKey;
    }

    private void parseXML(XmlPullParser parser, User user) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        Item currentItem = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String name = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    items = new ArrayList<Item>();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    //Log.d(logger, "Tag name: " + name);
                    if (name.equals("item")){
                        currentItem = new Item();
                        currentItem.user = user;
                    } else if (currentItem != null){
                        if (name.equals("title")){
                            currentItem.title = parser.nextText();
                        } else if (name.equals("link")){
                            currentItem.link = parser.nextText();
                        } else if (name.equals("pubDate")){
                            currentItem.pubDate = parser.nextText();
                        }  else if (name.equals("description")){
                            currentItem.description = parser.nextText();
                        }
                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("item") && currentItem != null){
                        items.add(currentItem);
                    }
            }
            eventType = parser.next();
        }
    }

    public void checkUpdates(final User user, final Action callback){
        AsyncHttpClient client = new AsyncHttpClient();
        String siteURL = "http://bato.to/myfollows_rss?secret=" + DataManager.getInstance().getDefaultSecretKey() + "&l=English";
        client.get(siteURL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] header, String result, Throwable e) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] header, String result) {
                InputStream is = new ByteArrayInputStream(result.getBytes());
                XmlPullParser parser = Xml.newPullParser();
                String parseResult = "";
                try {
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    parser.setInput(is, null);
                    parseXML(parser, user);

                    callback.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        });
    }

    public void getRssData(final ListView comicListView, final ArrayAdapter<String> adapter, final User user){
        checkUpdates(user, new Action() {
            @Override
            public void execute() {
                for(Item item: items){
                    adapter.add(item.title);
                }

                ((BaseAdapter) comicListView.getAdapter()).notifyDataSetChanged();
            }
        });
    }

    public ArrayList<Item> getItems(){
        return items;
    }

    public void save(ArrayList<Item> items){}
    public void load(String $query){}


}
