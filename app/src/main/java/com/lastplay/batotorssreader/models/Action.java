package com.lastplay.batotorssreader.models;

/**
 * Created by Harits Elfahmi on 4/9/2015.
 */
public interface Action {
    public void execute();
}
